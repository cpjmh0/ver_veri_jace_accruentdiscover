/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover;

import javax.baja.driver.util.BPollFrequency;
import javax.baja.status.BStatus;
import javax.baja.sys.*;

import com.tridium.ndriver.BNDevice;
import com.tridium.ndriver.poll.BINPollable;
import com.tridium.ndriver.util.SfUtil;

import com.accruent.accruentDiscover.point.*;


/**
 *  BADDevice models a single device
 *
 *  @author   mhayden
 *  @creation 22-May-17 
 */
public class BADDevice
  extends BNDevice
  implements BINPollable
{
  
  // Add facet to include following in auto manager view
  public static final Property status = newProperty(Flags.TRANSIENT|Flags.READONLY|Flags.SUMMARY|Flags.DEFAULT_ON_CLONE, BStatus.ok, SfUtil.incl(SfUtil.MGR_EDIT_READONLY));

  /*-
  class BADDevice
  {
    properties
    {
      pollFrequency : BPollFrequency
        --How frequently the device is polled for data
        default {[ BPollFrequency.normal ]}
       
      points : BADPointDeviceExt
        default {[ new BADPointDeviceExt() ]}
      
    }
    actions
    {
    }
    topics
    {
      
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.BADDevice(2020098393)1.0$ @*/
/* Generated Tue May 23 10:30:32 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "pollFrequency"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>pollFrequency</code> property.
   * How frequently the device is polled for data
   * @see com.accruent.accruentDiscover.BADDevice#getPollFrequency
   * @see com.accruent.accruentDiscover.BADDevice#setPollFrequency
   */
  public static final Property pollFrequency = newProperty(0, BPollFrequency.normal,null);
  
  /**
   * Get the <code>pollFrequency</code> property.
   * How frequently the device is polled for data
   * @see com.accruent.accruentDiscover.BADDevice#pollFrequency
   */
  public BPollFrequency getPollFrequency() { return (BPollFrequency)get(pollFrequency); }
  
  /**
   * Set the <code>pollFrequency</code> property.
   * How frequently the device is polled for data
   * @see com.accruent.accruentDiscover.BADDevice#pollFrequency
   */
  public void setPollFrequency(BPollFrequency v) { set(pollFrequency,v,null); }

////////////////////////////////////////////////////////////////
// Property "points"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>points</code> property.
   * @see com.accruent.accruentDiscover.BADDevice#getPoints
   * @see com.accruent.accruentDiscover.BADDevice#setPoints
   */
  public static final Property points = newProperty(0, new BADPointDeviceExt(),null);
  
  /**
   * Get the <code>points</code> property.
   * @see com.accruent.accruentDiscover.BADDevice#points
   */
  public BADPointDeviceExt getPoints() { return (BADPointDeviceExt)get(points); }
  
  /**
   * Set the <code>points</code> property.
   * @see com.accruent.accruentDiscover.BADDevice#points
   */
  public void setPoints(BADPointDeviceExt v) { set(points,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BADDevice.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  
////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////
  
  /**
   * Returns the network type that the device runs on.
   * @return Type object representing the network
   */
  public Type getNetworkType() { return BADNetwork.TYPE; }  
  
  
  /**
   * Override started 
   */
  public void started()
    throws Exception
  {
    super.started();
    
    // register device with poll scheduler 
    getADNetwork().getPollScheduler().subscribe(this);
  }
  
  /**
   * Override stopped 
   */
  public void stopped()
    throws Exception
  {
    // unregister device with poll scheduler 
    getADNetwork().getPollScheduler().unsubscribe(this);
    super.stopped();
  }
  
  
////////////////////////////////////////////////////////////////
// Implementation
////////////////////////////////////////////////////////////////
  /**
   * 
   */
  public void doPing() 
  { 
    // TODO - add ping implementation
    // if()
      pingOk();
    // else
    //  pingFail("not receiving response from device ");
  }
  

////////////////////////////////////////////////////////////////
// Polling support
////////////////////////////////////////////////////////////////

  /**
   * The poll() callback method called from BPollScheduler
   * when it is time to poll this object.
   */
  public void doPoll()
  {
    // TODO add poll support
  }
  
////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////
  /**
   * Get the network cast to a BADNetwork.
   * @return network as a BADNetwork.
   */
  public final BADNetwork getADNetwork()
  {
    return (BADNetwork)getNetwork();
  }

}
