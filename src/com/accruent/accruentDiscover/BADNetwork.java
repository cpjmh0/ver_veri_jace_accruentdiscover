/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover;

//import javax.baja.license.Feature;
import javax.baja.naming.BOrd;
import javax.baja.sys.*;
import javax.baja.util.Lexicon;

import com.accruent.accruentDiscover.comm.BAdTcpCommConfig;
import com.tridium.ndriver.BNNetwork;
import com.tridium.ndriver.comm.*;
import com.tridium.ndriver.datatypes.*;
import com.tridium.ndriver.discover.*;
import com.tridium.ndriver.poll.*;


/**
 *  BADNetwork models a network of devices
 *
 *  @author   mhayden
 *  @creation 22-May-17 
 */
public class BADNetwork 
  extends BNNetwork
{
  
  /*-
    class BADNetwork
    {
      properties
      {
        pollScheduler :  BNPollScheduler
          default {[ new BNPollScheduler() ]}
        tcpConfig : BAdTcpCommConfig
          default {[ new BAdTcpCommConfig() ]}
      }
    }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.BADNetwork(3474129191)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "pollScheduler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>pollScheduler</code> property.
   * @see com.accruent.accruentDiscover.BADNetwork#getPollScheduler
   * @see com.accruent.accruentDiscover.BADNetwork#setPollScheduler
   */
  public static final Property pollScheduler = newProperty(0, new BNPollScheduler(),null);
  
  /**
   * Get the <code>pollScheduler</code> property.
   * @see com.accruent.accruentDiscover.BADNetwork#pollScheduler
   */
  public BNPollScheduler getPollScheduler() { return (BNPollScheduler)get(pollScheduler); }
  
  /**
   * Set the <code>pollScheduler</code> property.
   * @see com.accruent.accruentDiscover.BADNetwork#pollScheduler
   */
  public void setPollScheduler(BNPollScheduler v) { set(pollScheduler,v,null); }

////////////////////////////////////////////////////////////////
// Property "tcpConfig"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>tcpConfig</code> property.
   * @see com.accruent.accruentDiscover.BADNetwork#getTcpConfig
   * @see com.accruent.accruentDiscover.BADNetwork#setTcpConfig
   */
  public static final Property tcpConfig = newProperty(0, new BAdTcpCommConfig(),null);
  
  /**
   * Get the <code>tcpConfig</code> property.
   * @see com.accruent.accruentDiscover.BADNetwork#tcpConfig
   */
  public BAdTcpCommConfig getTcpConfig() { return (BAdTcpCommConfig)get(tcpConfig); }
  
  /**
   * Set the <code>tcpConfig</code> property.
   * @see com.accruent.accruentDiscover.BADNetwork#tcpConfig
   */
  public void setTcpConfig(BAdTcpCommConfig v) { set(tcpConfig,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BADNetwork.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  
  
   

  /** Specify name for network resources. */
  public String getNetworkName() { return "ADNetwork"; }

  /** return device folder type  */
  public Type getDeviceFolderType()
  {
    return BADDeviceFolder.TYPE;
  }

  /** return device type */
  public Type getDeviceType()
  {
    return BADDevice.TYPE;
  }
  
  /** TODO - Add license check if needed
  public final Feature getLicenseFeature()
  {
    return Sys.getLicenseManager().getFeature("?? vendor", "?? feature");
  }
  */
  
////////////////////////////////////////////////////////////////
//Utilities
////////////////////////////////////////////////////////////////
  
  
  /**Access the tcp comm stack */
  public NComm tcomm()
  {
    return (NComm)getTcpConfig().comm();
  }
  
   
  
  
  public static Lexicon LEX = Lexicon.make(BADNetwork.class);

}
