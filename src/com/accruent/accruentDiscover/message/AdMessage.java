/**
 * Copyright 2013 - All Rights Reserved.
 */
package  com.accruent.accruentDiscover.message;

import com.tridium.ndriver.comm.*;
//import com.tridium.ndriver.io.*;
import com.tridium.ndriver.datatypes.*;

/**
 *  AdMessage is super class for all accruentDiscover messages
 *
 *  @author   mhayden
 *  @creation 22-May-17 
 */
public class AdMessage
  extends NMessage
{

  
  public AdMessage (BAddress address)
  {
    super(address);
  }
  
// Override for outgoing messages
//  public boolean toOutputStream(OutputStream out) 
//    throws Exception
//  {
//    // Use typed stream for more readable code.   
//    TypedOutputStream to = new TypedOutputStream(out);
//  }
    
//   Override for incoming messages
//  public void fromInputStream(InputStream in) 
//    throws Exception
//  {
//    // Use typed stream for more readable code.   
//    TypedInputStream ti = TypedInputStream(in);
//  }
    
//   Typical overrides  
//  public Object getTag() { return nullTag; }
//  public boolean isResponse() { return false; }
//  public boolean isFragmentable() { return false; }
//  public int getResponseTimeOut() { return 2500; }
    
//  public String toTraceString()
//  {
//    return DrByteArrayUtil.toString(??);
//  }

}
