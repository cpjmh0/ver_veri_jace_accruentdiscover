/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover.message;

import com.tridium.ndriver.comm.*;


/**
 * AdMessageFactory implementation of IMessageFactory.
 *
 * @author   mhayden
 * @creation 22-May-17 
 */
public class AdMessageFactory
  implements IMessageFactory
{
  
  public AdMessageFactory() {}
  
  public NMessage makeMessage(LinkMessage lm) 
      throws Exception
  {
    //
    // TODO - convert linkMessage driver specific NMessage
    return null;
  }

}
