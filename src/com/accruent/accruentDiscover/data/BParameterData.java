package com.accruent.accruentDiscover.data;

import javax.baja.sys.BComponent;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.WebOp;

public class BParameterData
    extends BComponent
{
  /*-
   class ParameterData
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.data.BParameterData(1610915591)1.0$ @*/
/* Generated Thu Jun 01 13:24:01 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BParameterData.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  String findComponentName = null;
  WebOp webop = null;
  public BParameterData()
  {
  }
  public BParameterData(String findComponentNamep, WebOp webopp)
  {
    this.findComponentName = findComponentNamep;
    this.webop = webopp;
  }
  public String getFindComponentName()
  {
    return findComponentName;
  }
  public void setFindComponentName(String findComponentName)
  {
    this.findComponentName = findComponentName;
  }
  public WebOp getWebop()
  {
    return webop;
  }
  public void setWebop(WebOp webop)
  {
    this.webop = webop;
  }


}
