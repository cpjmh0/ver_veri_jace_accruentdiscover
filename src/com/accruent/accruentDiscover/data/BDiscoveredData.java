package com.accruent.accruentDiscover.data;

import java.util.ArrayList;

import javax.baja.sys.BComponent;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

public class BDiscoveredData
    extends BComponent
{
  /*-
   class BDiscoveredData
   {
     properties
     {
         parentName : String
         default {[new String()]}
         
         componentName : String
         default {[new String()]}
         
         componentType : String
         default {[new String()]}
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.data.BDiscoveredData(678557724)1.0$ @*/
/* Generated Thu May 25 10:26:19 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "parentName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>parentName</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#getParentName
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#setParentName
   */
  public static final Property parentName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>parentName</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#parentName
   */
  public String getParentName() { return getString(parentName); }
  
  /**
   * Set the <code>parentName</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#parentName
   */
  public void setParentName(String v) { setString(parentName,v,null); }

////////////////////////////////////////////////////////////////
// Property "componentName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>componentName</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#getComponentName
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#setComponentName
   */
  public static final Property componentName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>componentName</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#componentName
   */
  public String getComponentName() { return getString(componentName); }
  
  /**
   * Set the <code>componentName</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#componentName
   */
  public void setComponentName(String v) { setString(componentName,v,null); }

////////////////////////////////////////////////////////////////
// Property "componentType"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>componentType</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#getComponentType
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#setComponentType
   */
  public static final Property componentType = newProperty(0, new String(),null);
  
  /**
   * Get the <code>componentType</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#componentType
   */
  public String getComponentType() { return getString(componentType); }
  
  /**
   * Set the <code>componentType</code> property.
   * @see com.accruent.accruentDiscover.data.BDiscoveredData#componentType
   */
  public void setComponentType(String v) { setString(componentType,v,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BDiscoveredData.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
   
   static ArrayList<BDiscoveredData> alDiscoveredData = null;
   
   public BDiscoveredData()
   {
     alDiscoveredData = new ArrayList<BDiscoveredData>();
   }
   
   public BDiscoveredData(String parentName, String componentName, String componentType, ArrayList<BDiscoveredData> alDiscoveredDatap)
   {
     this.setParentName(parentName);
     this.setComponentName(componentName);
     this.setComponentType(componentType);
     alDiscoveredDatap.add(this);
   }
   
   public void setAlDiscoveredData(ArrayList<BDiscoveredData> alDiscoveredDatap)
   {
     BDiscoveredData.alDiscoveredData = alDiscoveredDatap;
   }
   
   public ArrayList<BDiscoveredData> getAlDiscoveredData()
   {
     return alDiscoveredData;
   }
   
   
}
