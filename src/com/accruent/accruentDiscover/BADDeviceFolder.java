/*
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover;

import javax.baja.sys.*;

import com.tridium.ndriver.BNDeviceFolder;

/**
 * BADDeviceFolder is a folder for BADDevice.
 *
 *  @author   mhayden
 *  @creation 22-May-17 
 */
public class BADDeviceFolder
  extends BNDeviceFolder
{                       
/*-
  class BADDeviceFolder
  {
    properties
    {
    }
  }
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.BADDeviceFolder(1029552624)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BADDeviceFolder.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////

  /**
   * Get the network cast to a BADNetwork.
   * @return network as a BADNetwork.
   */
  public final BADNetwork getADNetwork()
  {
    return (BADNetwork)getNetwork();
  }
  
  /**
   * @return true if parent is BADNetwork or BADDeviceFolder.
   */
  public boolean isParentLegal(BComponent parent)
  {
    return parent instanceof BADNetwork ||
           parent instanceof BADDeviceFolder;
  }


}
