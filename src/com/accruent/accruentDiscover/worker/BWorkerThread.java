package com.accruent.accruentDiscover.worker;

import javax.baja.sys.NotRunningException;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.BWorker;
import javax.baja.util.CoalesceQueue;
import javax.baja.util.Worker;

public class BWorkerThread
    extends BWorker
{
  /*-
   class BWorkerThread
   {
     properties
     {
     }
     actions
     {

     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.worker.BWorkerThread(327953489)1.0$ @*/
/* Generated Mon May 22 16:02:31 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BWorkerThread.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  private CoalesceQueue queue = null;
  private Worker worker = null;
  
  
  public Worker getWorker()
  {
    if (worker == null)
    {
        queue = new CoalesceQueue(1000);
        worker = new Worker(queue);
    }
    return worker;
  }
  
  public void postAsync(Runnable r)
  {
    if(!isRunning() || queue == null)
    {
      throw new NotRunningException();
    }
    queue.enqueue(r);
  }

}
