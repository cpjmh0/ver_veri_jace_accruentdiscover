/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover.comm;

import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.tridium.ndriver.comm.IMessageFactory;
import com.tridium.ndriver.comm.LinkMessage;
import com.tridium.ndriver.comm.NLinkMessageFactory;
import com.tridium.ndriver.datatypes.BTcpCommConfig;

import com.accruent.accruentDiscover.message.AdMessageFactory;

/**
 * BAdTcpCommConfig Handles the tcp communication for the driver. 
 *
 * @author   mhayden
 * @creation 22-May-17 
 *
 */
public class BAdTcpCommConfig
  extends BTcpCommConfig
{
  /*-
  class BAdTcpCommConfig
  {
    properties
    {
    }
  }
 -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.comm.BAdTcpCommConfig(3114237498)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdTcpCommConfig.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  /** Empty constructor  */
  public BAdTcpCommConfig() {  }
  
//  /** Override to configure the maximum number of request messages that can
//   *  be outstanding at the same time.  Default implementation returns 32. */
//  public int getMaxOutstandingTransactions() { return 1; }

  /** Provide custom LinkMessage factory.   */
  protected NLinkMessageFactory makeLinkMessageFactory()
  {    
    // link message factory must create
    return new NLinkMessageFactory(1024)
      {
        protected LinkMessage createLinkMessage()
        {
          return new AdLinkMessage(1024); 
        }
      };
  }
  
  /** Provide custom Message factory.   */
  protected IMessageFactory makeMessageFactory()
  {
    return new AdMessageFactory();
  }
  
}
