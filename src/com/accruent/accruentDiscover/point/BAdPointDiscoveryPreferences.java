/*
 * copyright 2012 Tridium, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover.point;

import javax.baja.sys.*;

import com.tridium.ndriver.discover.*;

public class BAdPointDiscoveryPreferences
  extends BNDiscoveryPreferences
{
  /*-
    class BAdPointDiscoveryPreferences
    {
      properties
      {
      }
    }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.point.BAdPointDiscoveryPreferences(3283158918)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAdPointDiscoveryPreferences.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  public Type getDiscoveryLeafType()
  {
    return BAdPointDiscoveryLeaf.TYPE;
  }
  public boolean getNeedsJob() { return false; }

}
