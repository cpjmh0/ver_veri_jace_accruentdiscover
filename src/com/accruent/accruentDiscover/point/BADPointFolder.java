/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover.point;

import javax.baja.sys.*;
import javax.baja.driver.point.*;

import com.accruent.accruentDiscover.*;

/**
 * BADPointFolder
 *
 * @author   mhayden
 * @creation 22-May-17  
 */
public class BADPointFolder
  extends BPointFolder
{            
  /*-
  class BADPointFolder
  {
    properties
    {
    }
  }
 -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.point.BADPointFolder(2669510133)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BADPointFolder.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////
  
  /**
   * Get the network cast to a BADNetwork.
   * @return network as a BADNetwork.
   */
  public final BADNetwork getADNetwork()
  {
    return (BADNetwork)getNetwork();
  }

  /**
   * Get the device cast to a BADDevice.
   * @return device as a BADDevice.
   */
  public final BADDevice getADDevice()
  {
    return (BADDevice)getDevice();
  }

}
