/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover.point;

import javax.baja.sys.*;
import com.tridium.ndriver.discover.*;
import com.tridium.ndriver.point.*;

import com.accruent.accruentDiscover.*;

/**
 * BADPointDeviceExt is a container for accruentDiscover proxy points for.
 *
 * @author   mhayden
 * @creation 22-May-17 
 */
public class BADPointDeviceExt
  extends BNPointDeviceExt 
{            
  public static final Property discoveryPreferences = newProperty(0, new BAdPointDiscoveryPreferences(),null);

  /*-
    class BADPointDeviceExt
    {
      properties
      {
      }
    }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.point.BADPointDeviceExt(306496220)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BADPointDeviceExt.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////
  
  /**
   * Get the network cast to a BADNetwork.
   * @return network as a BADNetwork.
   */
  public final BADNetwork getADNetwork()
  {
    return (BADNetwork)getNetwork();
  }

  /**
   * Get the device cast to a BADDevice.
   * @return device as a BADDevice.
   */
  public final BADDevice getADDevice()
  {
    return (BADDevice)getDevice();
  }

////////////////////////////////////////////////////////////////
// PointDeviceExt
////////////////////////////////////////////////////////////////
  
  /**
   * @return the Device type.
   */
  public Type getDeviceType()
  {
    return BADDevice.TYPE;
  }

  /**
   * @return the PointFolder type.
   */
  public Type getPointFolderType()
  {
    return BADPointFolder.TYPE;
  }
  
  /**
   * @return the ProxyExt type.
   */
  public Type getProxyExtType()
  {
    return BADProxyExt.TYPE;
  }
  
////////////////////////////////////////////////////////////////
//BINDiscoveryHost
////////////////////////////////////////////////////////////////

  /** Call back for discoveryJob to get an array of discovery objects.
   *  Override point for driver specific discovery. */
  public BINDiscoveryObject[] getDiscoveryObjects(BNDiscoveryPreferences prefs) 
      throws Exception
  {
     //
     // TODO  get array of discovery objects
     //
//    Array a = new Array(??.class);
//    for(??)
//     a.add(new BAdPointDiscoveryLeaf(??));
//    return (??[])a.trim();
    return null;
  }
}
