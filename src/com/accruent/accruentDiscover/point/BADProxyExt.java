/**
 * Copyright 2017 accruent, Inc. All Rights Reserved.
 */
package com.accruent.accruentDiscover.point;

import javax.baja.sys.*;
import javax.baja.status.*;
import javax.baja.driver.point.*;

import com.accruent.accruentDiscover.*;
import com.tridium.driver.util.DrUtil;


/**
 * BADProxyExt
 *
 *  @author   mhayden
 * @creation 22-May-17 
 */
public class BADProxyExt
  extends BProxyExt
{   
  
  // Override ProxyExt default status to clear stale state.
  // public static final Property status = newProperty(Flags.READONLY|Flags.TRANSIENT, BStatus.ok, null);
  

  /*-             
  
  class BADProxyExt
  {
    properties
    {    
        
    }
  }
  
  -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.point.BADProxyExt(1901958687)1.0$ @*/
/* Generated Mon May 22 14:57:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BADProxyExt.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


  
////////////////////////////////////////////////////////////////
// Access
////////////////////////////////////////////////////////////////
  
  /**
   * Get the network cast to a BADNetwork.
   */
  public final BADNetwork getADNetwork()
  {
    return (BADNetwork)getNetwork();
  }

  /**
   * Get the device cast to a BADDevice.
   */
  public final BADDevice getBADDevice()
  {
    return (BADDevice)DrUtil.getParent(this, BADDevice.TYPE);
  }

  /**
   * Get the point device ext cast to a BADPointDeviceExt.
   */
  public final BADPointDeviceExt getADPointDeviceExt()
  {
    return (BADPointDeviceExt)getDeviceExt();
  }
  
////////////////////////////////////////////////////////////////
// ProxyExt
////////////////////////////////////////////////////////////////
  public void readSubscribed(Context cx)
    throws Exception
  {
    // TODO
  }
  
  public void readUnsubscribed(Context cx)
    throws Exception
  {
     // TODO
  }
  
  public boolean write(Context cx)
    throws Exception
  {
    // TODO
    return false;
  }
  
  /**
   * Return the device type. 
   */
  public Type getDeviceExtType()
  {
    return BADPointDeviceExt.TYPE;
  }                     
  
  /**
   * Return the read/write mode of this proxy.
   */
  public BReadWriteMode getMode()
  {
    // TODO
    return BReadWriteMode.readonly;
  }                        
  
  public boolean isBoolean()
  {
    return getParentPoint().getOutStatusValue() instanceof BStatusBoolean;
  }
  
  public boolean isNumeric()
  {
    return getParentPoint().getOutStatusValue() instanceof BStatusNumeric;
  }
  
  public boolean isString()
  {
    return getParentPoint().getOutStatusValue() instanceof BStatusString;
  }
  
  public boolean isEnum()
  {
    return getParentPoint().getOutStatusValue() instanceof BStatusEnum;
  }
  
}
