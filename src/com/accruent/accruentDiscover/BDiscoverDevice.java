package com.accruent.accruentDiscover;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.lonworks.proxy.BLonFloatProxyExt;
import javax.baja.sys.Action;
import javax.baja.sys.BComplex;
import javax.baja.sys.BComponent;
import javax.baja.sys.BStation;
import javax.baja.sys.BValue;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Slot;
import javax.baja.sys.Sys;
import javax.baja.sys.Topic;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;

import com.accruent.accruentDiscover.data.BDiscoveredData;
import com.accruent.accruentDiscover.worker.BWorkerThread;

public class BDiscoverDevice
    extends BComponent
    {
  /*-
   class BDiscoverDevice
   {
     properties
     {
        worker:BWorkerThread
        default {[new BWorkerThread()]}
        
     }
     actions
     {
        findDevice() : BDiscoveredData
        flags { async, summary }
     }
     topics
     {
         deviceDiscovered : BDiscoveredData
         flags  { summary }
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.BDiscoverDevice(2646368674)1.0$ @*/
/* Generated Thu May 25 10:42:31 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "worker"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>worker</code> property.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#getWorker
   * @see com.accruent.accruentDiscover.BDiscoverDevice#setWorker
   */
  public static final Property worker = newProperty(0, new BWorkerThread(),null);
  
  /**
   * Get the <code>worker</code> property.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#worker
   */
  public BWorkerThread getWorker() { return (BWorkerThread)get(worker); }
  
  /**
   * Set the <code>worker</code> property.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#worker
   */
  public void setWorker(BWorkerThread v) { set(worker,v,null); }

////////////////////////////////////////////////////////////////
// Action "findDevice"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>findDevice</code> action.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#findDevice()
   */
  public static final Action findDevice = newAction(Flags.ASYNC|Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>findDevice</code> action.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#findDevice
   */
  public BDiscoveredData findDevice() { return (BDiscoveredData)invoke(findDevice,null,null); }

////////////////////////////////////////////////////////////////
// Topic "deviceDiscovered"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>deviceDiscovered</code> topic.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#fireDeviceDiscovered
   */
  public static final Topic deviceDiscovered = newTopic(Flags.SUMMARY,null);
  
  /**
   * Fire an event for the <code>deviceDiscovered</code> topic.
   * @see com.accruent.accruentDiscover.BDiscoverDevice#deviceDiscovered
   */
  public void fireDeviceDiscovered(BDiscoveredData event) { fire(deviceDiscovered, event, null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BDiscoverDevice.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

private String findParent(BComplex component)
{
  StringBuffer sbParent = new StringBuffer();
  ArrayList<String> alParentName = new ArrayList<String>();
  while(component.getParent() != null)
  {
    alParentName.add(component.getName());
    component = component.getParent();
  }
  Collections.reverse(alParentName);
  for(String sName : alParentName)
  {
     if(null != sName)
     {
       sbParent.append(sName).append("-");
     }
  }
  
  return sbParent.toString().substring(0, sbParent.toString().length() - 1);
}
  
private BDiscoveredData findComponents(BComponent[] stationComponents, BDiscoveredData discoveredData, BufferedWriter bw ) throws Exception
{
  //Take out when done testing
  BComponent component = null;
  String parentComponent = null;
  for(int iCnt=0;iCnt<stationComponents.length;iCnt++)
  {
      component = stationComponents[iCnt];
      if(null != component && component.getChildComponents().length != 0) // we have components
      {
        /*
        if(null != component.getParent())
        {  
            //System.out.println("Parent1 = " + findParent(component));
          //parentComponent = findParent(component);
        }
        else
        {
          //System.out.println("Parent2 = " + component.getName());
          //discoveredData = new BDiscoveredData(parentComponent, component.getName());
        }
        */
        discoveredData = findComponents(component.getChildComponents(), discoveredData, bw);
      }
      else
      {
        parentComponent = findParent(component);
        if(component.getType().getTypeName().equals("LonFloatProxyExt"))
        {
          //System.out.println(String.format("Type Name = %s %s", component.getName(), component.getType().getTypeName()));
          bw.write(String.format("Type Name = %s %s\n", component.getName(), component.getType().getTypeName()));
          for(Slot slot : component.getSlotsArray())
          {
             if(slot.getName().equals("readValue"))
             {
               BLonFloatProxyExt bf = (BLonFloatProxyExt)slot.getDeclaringType().getInstance();
            //   System.out.println(String.format("%s %s", bf.getReadValue(), bf.getMode()));
             }
          }
        }   
        else
        {
         // System.out.println(String.format("Type Name = %s %s", component.getName(), component.getType().getTypeName()));
        }
        discoveredData = new BDiscoveredData(parentComponent, component.getName(), component.getType().getTypeName(), discoveredData.getAlDiscoveredData());
      }
  }
  return discoveredData;
}  
  
public BDiscoveredData  doFindDevice()
{
    FilePath fp = new FilePath("^AccruentData/discoveredData.txt");
    BIFile bf = null;
    BufferedWriter bw = null;
    BDiscoveredData discoveredData = new BDiscoveredData();
    BStation station = Sys.getStation();
    BComponent[] stationComponents = station.getChildComponents();
    try
    {
      bf = BFileSystem.INSTANCE.makeFile(fp);
      bw = new BufferedWriter(new OutputStreamWriter(bf.getOutputStream()));
      discoveredData = findComponents(stationComponents, discoveredData, bw);
      bw.flush();
      bw.close();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    fireDeviceDiscovered(discoveredData);
    return discoveredData;
}
  
  
public IFuture post(Action action, BValue argument, Context cx)
{
     getWorker().postAsync(new Invocation(this, action, argument, cx));
     return null;
}

}
 