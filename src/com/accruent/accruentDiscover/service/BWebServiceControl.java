package com.accruent.accruentDiscover.service;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.baja.control.BNumericWritable;
import javax.baja.status.BStatusNumeric;
import javax.baja.sys.Action;
import javax.baja.sys.BComponent;
import javax.baja.sys.BDouble;
import javax.baja.sys.Flags;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;

import com.accruent.accruentDiscover.BDiscoverDevice;
import com.accruent.accruentDiscover.data.BDiscoveredData;

public class BWebServiceControl
    extends BWebServlet
{
  /*-
   class BWebServiceControl
   {
     properties
     {
     }
     actions
     {
        listenForCommand() : BDiscoveredData
        flags {summary}
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.service.BWebServiceControl(2638707041)1.0$ @*/
/* Generated Thu May 25 10:28:29 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Action "listenForCommand"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>listenForCommand</code> action.
   * @see com.accruent.accruentDiscover.service.BWebServiceControl#listenForCommand()
   */
  public static final Action listenForCommand = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>listenForCommand</code> action.
   * @see com.accruent.accruentDiscover.service.BWebServiceControl#listenForCommand
   */
  public BDiscoveredData listenForCommand() { return (BDiscoveredData)invoke(listenForCommand,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BWebServiceControl.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  @Override
  public void doGet(WebOp webOp)
  {
    BDiscoveredData discoveredData = null;
    System.out.println("DoGet");
    try
    {
       webOp.getResponse().getWriter().write("In the Do Get Method");
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    discoveredData = doListenForCommand();
    try
    {
      
      for (BDiscoveredData discoveredDat : discoveredData.getAlDiscoveredData())
      {
        webOp.getResponse().getWriter().write(String.format("Name = %s Component = %s Type = %s\n <br>", 
                                         discoveredDat.getParentName(),
                                         discoveredDat.getComponentName(), 
                                         discoveredDat.getComponentType()));
      }
      
      
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }
  
  
  @Override
  public void doPost(WebOp webOp)
  {
    BDiscoveredData discoveredData = null;
    System.out.println("DoPost");
    String parameterName = null;
    String[] remoteChannelIds = null;
    List<String> alRemoteChannelIds = null;
    try
    {
       webOp.getResponse().getWriter().write("In the Do Post Method");
       for(Enumeration<String> parameters = webOp.getRequest().getParameterNames(); parameters.hasMoreElements();)
       {
           parameterName = parameters.nextElement();
           if(parameterName.equals("remoteChannelIds"))
           {
              remoteChannelIds = webOp.getRequest().getParameter(parameterName).split("\\|");
           }   
       }
       alRemoteChannelIds = Arrays.asList(remoteChannelIds);
       if(null != remoteChannelIds)
       {
         for(BComponent bStationComponents : Sys.getStation().getChildComponents())
         {
           if(bStationComponents.getName().equals("Drivers"))
           {
             for(BComponent bDrivers : bStationComponents.getChildComponents())
             {
               if(bDrivers.getName().equals("LonNetwork"))
               {
                  for(BComponent bDevices : bDrivers.getChildComponents())
                  {
                    {
                      for(BComponent bChannels : bDevices.getChildComponents())
                      {
                        if(bChannels.getName().equals("points"))
                        {
                           for(BComponent bPoint : bChannels.getChildComponents())
                           {
                             if(alRemoteChannelIds.contains(bPoint.getNavOrd().toString().split("\\|")[2]))
                             {  
                                System.out.println(String.format("Point = %s %s", bPoint.getName(), bPoint.getNavOrd()));
                                BNumericWritable bnw = (BNumericWritable)bPoint.asComponent();
                                bnw.set(BDouble.make(71.0));
                                bnw.doSet(BDouble.make(80.0));
                                bnw.setIn10(new BStatusNumeric(72.0));
                                System.out.println(String.format("%s %s %s", bPoint.isMounted(), bPoint.isRunning(), bPoint.isSubscribed()));
                             }   
                           }
                        }
                      }
                    }
                  }
               }
             }
           }
         }
       }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    discoveredData = doListenForCommand();
    try
    {
      for (BDiscoveredData discoveredDat : discoveredData.getAlDiscoveredData())
      {
       // webOp.getResponse().getWriter().write(String.format("Name = %s Component = %s Type = %s\n <br>", 
       //                                  discoveredDat.getParentName(),
       //                                  discoveredDat.getComponentName(), 
       //                                  discoveredDat.getComponentType()));
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  @Override
  public void serviceStarted()
  {
    this.setServletName("webServiceControl");
  }
  
  public BDiscoveredData doListenForCommand()
  {
    BDiscoverDevice discoverDevice = new BDiscoverDevice();
    BDiscoveredData discoveredData = new BDiscoveredData();
    System.out.println("Listen For Command");
    discoveredData = discoverDevice.doFindDevice();
    return discoveredData;
  }
  
  
  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }

}
