package com.accruent.accruentDiscover.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;

public class BUpdateActivePointsFile
    extends BWebServlet
{
  /*-
   class BUpdateActivePointsFile
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.service.BUpdateActivePointsFile(1414030061)1.0$ @*/
/* Generated Thu Jun 01 16:31:26 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BUpdateActivePointsFile.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  @Override
  public void doGet(WebOp webOp)
  {
     String fileList = webOp.getRequest().getParameter("file_list");
     try
     {
        webOp.getResponse().getWriter().println(fileList);
     }
     catch(Exception e)
     {
       e.printStackTrace();
     }
  }  
  
  
@Override
public void doPost(WebOp webOp)
{
}

@Override
public void serviceStarted()
{
  this.setServletName("updateActivePointsFile");
}

public Type[] getServiceTypes()
{
  return new Type[] {TYPE};
}


}
