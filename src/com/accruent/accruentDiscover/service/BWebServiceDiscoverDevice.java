package com.accruent.accruentDiscover.service;

import java.util.ArrayList;
import java.util.Collections;

import javax.baja.sys.Action;
import javax.baja.sys.BComplex;
import javax.baja.sys.BComponent;
import javax.baja.sys.BStation;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;

import com.accruent.accruentDiscover.worker.BWorkerThread;

import com.accruent.accruentDiscover.data.BParameterData;
import com.accruent.accruentDiscover.data.BDiscoveredData;


public class BWebServiceDiscoverDevice
    extends BWebServlet
{
  /*-
   class BWebServiceDiscoverDevice
   {
     properties
     {
        worker:BWorkerThread
        default {[new BWorkerThread()]}
     }
     actions
     {
        discoverDevices(arg:BParameterData) : BDiscoveredData
        default {[new BParameterData()]} 
        flags {summary}
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice(879588168)1.0$ @*/
/* Generated Thu Jun 01 14:39:07 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "worker"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>worker</code> property.
   * @see com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice#getWorker
   * @see com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice#setWorker
   */
  public static final Property worker = newProperty(0, new BWorkerThread(),null);
  
  /**
   * Get the <code>worker</code> property.
   * @see com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice#worker
   */
  public BWorkerThread getWorker() { return (BWorkerThread)get(worker); }
  
  /**
   * Set the <code>worker</code> property.
   * @see com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice#worker
   */
  public void setWorker(BWorkerThread v) { set(worker,v,null); }

////////////////////////////////////////////////////////////////
// Action "discoverDevices"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>discoverDevices</code> action.
   * @see com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice#discoverDevices()
   */
  public static final Action discoverDevices = newAction(Flags.SUMMARY,new BParameterData(),null);
  
  /**
   * Invoke the <code>discoverDevices</code> action.
   * @see com.accruent.accruentDiscover.service.BWebServiceDiscoverDevice#discoverDevices
   */
  public BDiscoveredData discoverDevices(BParameterData arg) { return (BDiscoveredData)invoke(discoverDevices,arg,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BWebServiceDiscoverDevice.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/
  
  @Override
  public void doPost(WebOp webOp)
  {
    BParameterData parameterData = null;
    String findComponentName = null;
    if(null != webOp.getRequest().getParameter("find_component"))
    {
      findComponentName = webOp.getRequest().getParameter("find_component");
      parameterData = new BParameterData(findComponentName, webOp);
      doDiscoverDevices(parameterData);
    }
  }

  
  @Override
  public void serviceStarted()
  {
     this.setServletName("discoverDevices");
  }
  
  @Override
  public void serviceStopped()
  {
    //we want to delete the configuration file here so it will be rebuilt during the next telementtry push
  }
  
  public BDiscoveredData doDiscoverDevices(BParameterData parameterData)
  {
    BDiscoveredData discoveredData = new BDiscoveredData();
    BStation station = Sys.getStation();
    BComponent[] stationComponents = station.getChildComponents();
    for(BComponent stationComponent : stationComponents)
    {
      if(null != parameterData.getFindComponentName())
      {
         if(parameterData.getFindComponentName().equals(stationComponent.getName()))
         {
           findComponents(stationComponent, discoveredData);
         }
      }
      else
      {
        findComponents(stationComponent, discoveredData);
      }
    }
    System.out.println("Found Components " + discoveredData.getAlDiscoveredData().size());
    try
    {
      for (BDiscoveredData discoveredDat : discoveredData.getAlDiscoveredData())
      {
        parameterData.getWebop().getResponse().getWriter().write(String.format("%s^%s^%s\n", 
                                         discoveredDat.getParentName(),
                                         discoveredDat.getComponentName(), 
                                         discoveredDat.getComponentType()));
        if(discoveredData.getParentName().split("\\^").length > 3 && discoveredData.getParentName().split("\\^")[3].equals("points") && discoveredDat.getComponentName().equals("proxyExt"))
        {
          System.out.println(String.format("Parent = %s Component = %s Type = %s", discoveredDat.getParentName(), discoveredDat.getComponentName(), discoveredDat.getComponentType()));
        }
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    return discoveredData;
  }
  
  private String findParent(BComplex component)
  {
    StringBuffer sbParent = new StringBuffer();
    ArrayList<String> alParentName = new ArrayList<String>();
    while(component.getParent() != null)
    {
      alParentName.add(component.getName());
      component = component.getParent();
    }
    Collections.reverse(alParentName);
    for(String sName : alParentName)
    {
       if(null != sName)
       {
         sbParent.append(sName).append("^");
       }
    }
    
    return sbParent.toString().substring(0, sbParent.toString().length() - 1);
  }
    
  private BDiscoveredData findComponents(BComponent findComponent, BDiscoveredData discoveredData)
  {
    String parentComponent = null;
    for (BComponent component : findComponent.getChildComponents())
    {
      if (null != component && component.getChildComponents().length != 0) // we have components
      {
        discoveredData = findComponents(component, discoveredData);
      }
      else
      {
        parentComponent = findParent(component);
        discoveredData = new BDiscoveredData(parentComponent, component.getName(), component.getType().getTypeName(), discoveredData.getAlDiscoveredData());

      }
    }
    return discoveredData;
  }
  
  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }

  class DiscoveredData
  {
      String parent = null;
      String component = null;
      String type = null;
      DiscoveredData(String parentp, String componentp, String typep)
      {
        this.parent = parentp;
        this.component = componentp;
        this.type = typep;
      }
      public String getParent()
      {
        return parent;
      }
      public void setParent(String parent)
      {
        this.parent = parent;
      }
      public String getComponent()
      {
        return component;
      }
      public void setComponent(String component)
      {
        this.component = component;
      }
      public String getType()
      {
        return type;
      }
      public void setType(String type)
      {
        this.type = type;
      }
  }
}

 
