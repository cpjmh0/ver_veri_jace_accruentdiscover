package com.accruent.accruentDiscover.service;


import javax.baja.sys.Action;
import javax.baja.sys.BAbstractService;
import javax.baja.sys.Flags;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;

import com.accruent.accruentDiscover.data.BDiscoveredData;

public class BDiscoverService
    extends BAbstractService
{
  /*-
   class BDiscoverService
   {
     properties
     {

     }
     actions
     {
        fireWebService(arg: BDiscoveredData) :BDiscoveredData
        default {[new BDiscoveredData()]}
        flags {summary}
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.accruentDiscover.service.BDiscoverService(1753096084)1.0$ @*/
/* Generated Wed Jun 28 14:23:14 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Action "fireWebService"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>fireWebService</code> action.
   * @see com.accruent.accruentDiscover.service.BDiscoverService#fireWebService()
   */
  public static final Action fireWebService = newAction(Flags.SUMMARY,new BDiscoveredData(),null);
  
  /**
   * Invoke the <code>fireWebService</code> action.
   * @see com.accruent.accruentDiscover.service.BDiscoverService#fireWebService
   */
  public BDiscoveredData fireWebService(BDiscoveredData arg) { return (BDiscoveredData)invoke(fireWebService,arg,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BDiscoverService.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  public BDiscoveredData doFireWebService(BDiscoveredData discoveredDatas)
  {
    System.out.println("Length = " + discoveredDatas.getAlDiscoveredData().size());
    if (null != discoveredDatas.getAlDiscoveredData())
    {
      for (BDiscoveredData discoveredData : discoveredDatas.getAlDiscoveredData())
      {
        System.out.println(String.format("Name = %s Component = %s Type = %s", 
                                         discoveredData.getParentName(),
                                         discoveredData.getComponentName(), 
                                         discoveredData.getComponentType()));
      }
    }
    else
    {
      System.out.println("Null array list");
    }
    return discoveredDatas;
  }
  
  public void doListenForCommand()
  {
    System.out.println("Listen For Command");
  }
  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }

}
